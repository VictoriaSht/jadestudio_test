// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "JadeStudio_TestGame/DataTypes/Enums.h"
#include "CharacterBase.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class UCameraComponent;
class UInputAction;
class UInputMappingContext;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogCharacter, Log, All);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUpdateAmmoWidget);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHideAmmoWidget);

UCLASS(config=Game)
class ACharacterBase : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Mesh, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	/** Third person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* ThirdPersonCameraComponent;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Toggle View Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* ToggleViewAction;
	
	/** Fire Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* FireAction;

	/** Show Stats Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* DropWeaponAction;

	/** Show Stats Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* StatsAction;
public:
	/**  Health component*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	class UHealthComponent* HealthComponent;

	// New weapon event delegate for widget
	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FOnUpdateAmmoWidget OnUpdateAmmoWidget;
	// Weapon dropped event delegate for widget
	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FOnHideAmmoWidget OnHideAmmoWidget;

	ACharacterBase();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:
	/** For setting up binding and meshes visibility */
	virtual void PossessedBy(AController* NewController) override; 

	/**Set shoot location same for server from client data*/
	UFUNCTION(Server, Unreliable)
	void SetPitch_OnServer(float NewPitch);

	/**Set shoot location same for server from client data*/
	UFUNCTION(NetMulticast, Unreliable)
	void SetPitch_Multicast(float NewPitch);

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* LookAction;

	/** Bool for AnimBP to switch to another animation set */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	bool bHasWeapon;

	/** Setter to set the bool */
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void SetHasWeapon(bool bNewHasRifle);

	/** Getter for the bool */
	UFUNCTION(BlueprintCallable, Category = Weapon)
	bool GetHasWeapon() const;

	UFUNCTION(BlueprintCallable, Category = Weapon)
	class AWeaponBase* GetCurrentWeapon();

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = Weapon)
	void SetCurrentWeapon_OnServer(class AWeaponBase* NewWeapon);

	/** Try fire weapon */
	void InputAttackPressed();
	/** Fire inut released */
	void InputAttackReleased();

	UFUNCTION(BlueprintCallable)
	void ChangeWeaponState(bool bIsFiring);

	// Pitch for animation modifying
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float CurrentPitch;

protected:
	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	/** Toggle camera view (1'st person / 3'rd person) **/
	void ToggleView();

	/** Drop current weapon if valid */
	void DropWeapon();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

public:
	/** Death event */
	UFUNCTION(BlueprintCallable, Category = "Health")
	void OnDead();

	/**  Characted has died */
	UFUNCTION(BlueprintNativeEvent, Category = "Health")
	void CharDead_BP(AController* Killer);

	/**  Drop weapon to world (handled in BP) */
	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")
	void DropWeaponToWorld(EWeaponType WeaponType, int32 WeaponRound);

	/**  Drop weapon */
	UFUNCTION(Server, Reliable, Category = "Weapon")
	void DropWeapon_OnServer();

	/**  Reloading weapon bind func */
	UFUNCTION()
	void StartReloadingWeapon();

	/**  Reloading weapon end bind func */
	UFUNCTION()
	void EndReloadingWeapon(bool Success);

private:
	/**  Death anim montages */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", meta = (AllowPrivateAccess = "true"))
	TArray<UAnimMontage*> DeathAnims;

	/**  Reloading anim montage third person */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ReloadingAnimThirdPerson;
	/**  Reloading anim montages first person */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ReloadingAnimFirstPerson;

	UFUNCTION(NetMulticast, Reliable, Category = "Animation")
	void PlayAnimationOnBothMeshes_Multicast(UAnimMontage* ThirdPersonAnimation, UAnimMontage* FirstPersonAnim);

	UFUNCTION(NetMulticast, Reliable, Category = "Animation")
	void StopAnimationOnBothMeshes_Multicast(UAnimMontage* ThirdPersonAnimation, UAnimMontage* FirstPersonAnim);

public:
	/**  Set fullbody mesh visibility */
	UFUNCTION(NetMulticast, Reliable, Category = Gameplay)
	void SetMeshVisibility_Multicast();

private:
	/**  If first person view is enabled */
	bool bInFirstPersonView;

	/**  Current weapon */
	UPROPERTY(ReplicatedUsing = CurrentWeapon_OnRep)
	class AWeaponBase* CurrentWeapon;
	UFUNCTION()
	void CurrentWeapon_OnRep();

protected:
	/**  Effects */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Effects")
	TArray<class UStatusEffect*> Effects;

public:
	/** Add effect */
	void AddEffect(class UStatusEffect* AddedEffect);
	/** Remove effect */
	void RemoveEffect(class UStatusEffect* RemovedEffect);

private:
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UStatusEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UStatusEffect* EffectRemove = nullptr;

	//Effect added rep notify
	UFUNCTION()
	void EffectAdd_OnRep();
	//Effect removed rep notify
	UFUNCTION()
	void EffectRemove_OnRep();

	// Create niagara system or destroy it
	UFUNCTION()
	void HandleEffectsFX(class UStatusEffect* Effect, bool IsAdded);

	// Effect's FX
	TArray<class UNiagaraComponent*> FXEffects;
	// Effect's FX
	TArray<class UAudioComponent*> AudioEffects;

};

