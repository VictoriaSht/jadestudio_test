// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "JadeStudio_TestGame/DataTypes/Enums.h"
#include "PlayerStateBase.generated.h"

/**
 * 
 */
UCLASS()
class JADESTUDIO_TESTGAME_API APlayerStateBase : public APlayerState
{
	GENERATED_BODY()
	

public:
	// Get this player's team
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	EPlayerTeams GetTeam() const;

protected:
	// Set player team
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void SetTeam(EPlayerTeams NewTeam);

private:
	// This player's team
	UPROPERTY(ReplicatedUsing = TeamChange_OnRep)
	EPlayerTeams MyTeam;
	UFUNCTION()
	void TeamChange_OnRep();

public:
	// Update stats widget when player chooses team
	UFUNCTION(BlueprintNativeEvent, Category = "Stats")
	void UpdateStats();

};
