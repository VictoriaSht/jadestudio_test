// Copyright Epic Games, Inc. All Rights Reserved.

#include "JadeStudio_TestGame/Player/CharacterBase.h"
#include "JadeStudio_TestGame/Weapon/ProjectileBase.h"
#include "JadeStudio_TestGame/Weapon/WeaponBase.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Engine/LocalPlayer.h"
#include "HealthComponent.h"
#include "JadeStudio_TestGame/Effects/StatusEffect.h"
#include "Net/UnrealNetwork.h"
#include "Components/ArrowComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

DEFINE_LOG_CATEGORY(LogCharacter);

//////////////////////////////////////////////////////////////////////////
// ACharacterBase

ACharacterBase::ACharacterBase()
{
	// Character doesnt have a rifle at start
	bHasWeapon = false;
	
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
		
	// Creating a First Person CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Creating a Third Person CameraComponent	
	ThirdPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("ThirdPersonCamera"));
	ThirdPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	ThirdPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	ThirdPersonCameraComponent->bUsePawnControlRotation = true;

	// Creating a mesh component that will be used when being viewed from a '1st person' view
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeLocation(FVector(-30.f, 0.f, -150.f));

	// Create health component...
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

	// NetWork
	bReplicates = true;
}

void ACharacterBase::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
}

void ACharacterBase::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	if (IsLocallyControlled())
	{
		FRotator CameraRot = FirstPersonCameraComponent->GetComponentRotation();
		CurrentPitch = CameraRot.Pitch;
		SetPitch_OnServer(CurrentPitch);
	}
}

void ACharacterBase::SetPitch_OnServer_Implementation(float NewPitch)
{
	SetPitch_Multicast(NewPitch);
}

void ACharacterBase::SetPitch_Multicast_Implementation(float NewPitch)
{
	if (!IsLocallyControlled())
	{
		CurrentPitch = NewPitch;
	}
}


bool ACharacterBase::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}

	return Wrote;
}

void ACharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	// Set correct mesh visibility when posessed
	SetMeshVisibility_Multicast();
}

void ACharacterBase::SetMeshVisibility_Multicast_Implementation()
{
	// Destroy fullbody mesh that is not needed in '1st person' view
	if (GetController() && GetController()->IsLocalPlayerController() && GetMesh())
	{
		GetMesh()->SetVisibility(false);
	}
}


//////////////////////////////////////////////////////////////////////////// Input

void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	APlayerController* myPC = Cast<APlayerController>(GetController());

	//Get the local player subsystem
	UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(myPC->GetLocalPlayer());

	//Clear existing mappings and add our mapping
	Subsystem->ClearAllMappings();
	Subsystem->AddMappingContext(DefaultMappingContext, 0);

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ACharacterBase::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ACharacterBase::Look);

		// Toggle View
		EnhancedInputComponent->BindAction(ToggleViewAction, ETriggerEvent::Started, this, &ACharacterBase::ToggleView);

		// Fire
		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Started, this, &ACharacterBase::InputAttackPressed);
		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Completed, this, &ACharacterBase::InputAttackReleased);

		// Drop weapon
		EnhancedInputComponent->BindAction(DropWeaponAction, ETriggerEvent::Started, this, &ACharacterBase::DropWeapon);
	}
	else
	{
		UE_LOG(LogCharacter, Error, TEXT("ACharacterBase::SetupPlayerInputComponent - '%s' Failed to find an Enhanced Input Component!"), *GetNameSafe(this));
	}
}

void ACharacterBase::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void ACharacterBase::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ACharacterBase::DropWeapon()
{
	DropWeapon_OnServer();
}

// Fire imput pressed
void ACharacterBase::InputAttackPressed()
{
	if (HealthComponent->IsAlive())
		ChangeWeaponState(true);
}

// Fire input released
void ACharacterBase::InputAttackReleased()
{
	ChangeWeaponState(false);
}


//////////////////////////////////////////////////////////////////////////// Weapon
void ACharacterBase::SetHasWeapon(bool bNewHasRifle) {	bHasWeapon = bNewHasRifle;	}
bool ACharacterBase::GetHasWeapon() const {	return bHasWeapon;	}
AWeaponBase* ACharacterBase::GetCurrentWeapon() {	return CurrentWeapon;	}

// New weapon picked up
void ACharacterBase::SetCurrentWeapon_OnServer_Implementation(AWeaponBase* NewWeapon)	
{	
	CurrentWeapon = NewWeapon;	
	SetHasWeapon(CurrentWeapon != nullptr);

	if (CurrentWeapon != nullptr)
		OnUpdateAmmoWidget.Broadcast();
	else
		OnHideAmmoWidget.Broadcast();
}

// Current weapon rep notify
void ACharacterBase::CurrentWeapon_OnRep()
{
	if (CurrentWeapon != nullptr)
		OnUpdateAmmoWidget.Broadcast();
	else
		OnHideAmmoWidget.Broadcast();
}

// Called from fire input events
void ACharacterBase::ChangeWeaponState(bool bIsFiring)
{
	// Get current weapon and set correct firing state
	AWeaponBase* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
}

void ACharacterBase::DropWeapon_OnServer_Implementation()
{
	AWeaponBase* Weapon = GetCurrentWeapon();
	if (Weapon)
	{
		// Stop reloading
		GetCurrentWeapon()->CancelReload();

		// Spawn pick up actor
		DropWeaponToWorld(Weapon->WeaponType, GetCurrentWeapon()->GetWeaponRound());

		// Destroy old weapon
		GetCurrentWeapon()->Destroy();
		SetCurrentWeapon_OnServer(nullptr);
	}
}

void ACharacterBase::DropWeaponToWorld_Implementation(EWeaponType WeaponType, int32 WeaponRound)
{
}

void ACharacterBase::StartReloadingWeapon()
{
	PlayAnimationOnBothMeshes_Multicast(ReloadingAnimThirdPerson, ReloadingAnimFirstPerson);
}

void ACharacterBase::EndReloadingWeapon(bool Success)
{
	StopAnimationOnBothMeshes_Multicast(ReloadingAnimThirdPerson, ReloadingAnimFirstPerson);
}


//////////////////////////////////////////////////////////////////////////// Effects
void ACharacterBase::AddEffect(UStatusEffect* AddedEffect)
{
	Effects.Add(AddedEffect);

	HandleEffectsFX(AddedEffect, true);
	EffectAdd = AddedEffect;
}

void ACharacterBase::RemoveEffect(UStatusEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);

	HandleEffectsFX(RemovedEffect, false);
	EffectRemove = RemovedEffect;
}

void ACharacterBase::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		HandleEffectsFX(EffectAdd, true);
	}
}

void ACharacterBase::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		HandleEffectsFX(EffectRemove, false);
	}
}

void ACharacterBase::HandleEffectsFX(UStatusEffect* Effect, bool IsAdded)
{
	UStatusEffect_WithDuration* EffectTimer = Cast<UStatusEffect_WithDuration>(Effect);
	if (EffectTimer && EffectTimer->DurationEffect)
	{
		if (IsAdded)
		{
			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UNiagaraComponent* myParticle = UNiagaraFunctionLibrary::SpawnSystemAttached(EffectTimer->DurationEffect, myMesh, Effect->NameBone, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				FXEffects.Add(myParticle);
			}
		}
		else
		{
			for (UNiagaraComponent* elem : FXEffects)
			{
				if (elem && elem->GetAsset() && EffectTimer->DurationEffect == elem->GetAsset())
				{
					elem->Deactivate();
					elem->DestroyComponent();
				}
			}
		}
	}
	//if (EffectTimer && EffectTimer->DurationSound)
	//{
	//	if (IsAdded)
	//	{
	//		UAudioComponent* DurationAudioComponent = UGameplayStatics::SpawnSoundAttached(EffectTimer->DurationSound, this->GetRootComponent(), Effect->NameBone, FVector(0), FRotator(0), EAttachLocation::SnapToTarget);
	//		AudioEffects.Add(DurationAudioComponent);
	//	}
	//	else
	//	{
	//		for (UAudioComponent* elem : AudioEffects)
	//		{
	//			if (elem->GetSound() && elem->GetSound() == EffectTimer->DurationSound)
	//			{
	//				elem->FadeOut(0.5f, 0);
	//				elem->DestroyComponent();
	//			}
	//		}
	//	}
	//}
}

//////////////////////////////////////////////////////////////////////////// View
void ACharacterBase::ToggleView()
{
	if (IsLocallyControlled())
	{
		if (bInFirstPersonView)
		{
			// Set correct mesh to be visible
			if (GetMesh())
				GetMesh()->SetVisibility(true);
			if (Mesh1P)
				Mesh1P->SetVisibility(false);

			// Set correct camera to be active
			FirstPersonCameraComponent->SetActive(false);
			ThirdPersonCameraComponent->SetActive(true);
			bInFirstPersonView = false;
		}
		else
		{
			// Set correct mesh to be visible
			if (GetMesh())
				GetMesh()->SetVisibility(false);
			if (Mesh1P)
				Mesh1P->SetVisibility(true);

			// Set correct camera to be active
			ThirdPersonCameraComponent->SetActive(false);
			FirstPersonCameraComponent->SetActive(true);
			bInFirstPersonView = true;
		}
		// Reattach weapon mesh
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->ReAttachWeapon_OnServer(this, bInFirstPersonView);
		}
	}
}

//////////////////////////////////////////////////////////////////////////// Health
void ACharacterBase::OnDead()
{
	if (HasAuthority())
	{
		float AnimTime = 0.0f;
		int32 rnd = FMath::RandHelper(DeathAnims.Num());
		if (DeathAnims.IsValidIndex(rnd) && DeathAnims[rnd] && GetMesh())
		{
			if (GetMesh() && GetMesh()->HasValidAnimationInstance())
			{
				AnimTime = DeathAnims[rnd]->GetPlayLength();
				//PlayAnimation_Multicast(DeathAnims[rnd]);
			}
			else
				UE_LOG(LogTemp, Error, TEXT("ATDS_ViktoriaCharacter::CharDead - CharacterMesh anim instance -NULL"));
		}
		// Destroy char after death animation
		SetLifeSpan(AnimTime);

		// Destroy effects
		for (int i = Effects.Num() - 1; i >= 0; i--)
		{
			Effects[i]->DestroyObject();
		}

		// Drop weapon
		DropWeapon_OnServer();

		// For BP
		CharDead_BP(HealthComponent->GetLastDamageInstigator());
	}
	else
	{
		// Cancel fire
		ChangeWeaponState(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
	}


}

void ACharacterBase::CharDead_BP_Implementation(AController* Killer)
{
}

//////////////////////////////////////////////////////////////////////////// Animation

void ACharacterBase::PlayAnimationOnBothMeshes_Multicast_Implementation(UAnimMontage* ThirdPersonAnimation, UAnimMontage* FirstPersonAnim)
{
	if (IsLocallyControlled())
	{
		if (Mesh1P && Mesh1P->HasValidAnimationInstance())
		{
			Mesh1P->GetAnimInstance()->Montage_Play(FirstPersonAnim);
		}
	}
	
	if (GetMesh() && GetMesh()->HasValidAnimationInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(ThirdPersonAnimation);
	}
}

void ACharacterBase::StopAnimationOnBothMeshes_Multicast_Implementation(UAnimMontage* ThirdPersonAnimation, UAnimMontage* FirstPersonAnim)
{
	if (IsLocallyControlled())
	{
		if (Mesh1P && Mesh1P->HasValidAnimationInstance())
		{
			Mesh1P->GetAnimInstance()->Montage_Stop(0.1f, FirstPersonAnim);
		}
	}

	if (GetMesh() && GetMesh()->HasValidAnimationInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Stop(0.1f, ThirdPersonAnimation);
	}
}

//////////////////////////////////////////////////////////////////////////// Replication
void ACharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACharacterBase, CurrentWeapon);
	DOREPLIFETIME(ACharacterBase, bHasWeapon);
	DOREPLIFETIME(ACharacterBase, Effects);
	DOREPLIFETIME(ACharacterBase, EffectAdd);
	DOREPLIFETIME(ACharacterBase, EffectRemove);
}