// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "CharacterBase.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// We don't need tick here
	PrimaryComponentTick.bCanEverTick = false;

	// Player is alive at start
	bIsAlive = true;

	// Set max health
	Health = MaxHealth;
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// Bind func to owner damage event
	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OwnerGotDamaged);

	// Bind owner's death event
	ACharacterBase* OwnerCharacter = Cast<ACharacterBase>(GetOwner());
	if (OwnerCharacter)
	{
		//OnDead.AddDynamic(OwnerCharacter, &ACharacterBase::OnDead);
	}
	
}


float UHealthComponent::GetHealth() const
{
	return Health;
}

void UHealthComponent::SetHealth(float NewHealth)
{
	Health = NewHealth;
	OnHealthChangeEvent_Multicast(Health, 0);
}

// Increasing health
void UHealthComponent::DecreaseHealth_OnServer_Implementation(float DamageValue)
{
	// If owner is dead or damage is zero, do nothing
	if (!bIsAlive || DamageValue == 0)
	{
		return;
	}

	// Take Damage
	Health -= DamageValue; 
	OnHealthChangeEvent_Multicast(Health, -DamageValue);
	if (Health <= 0)
	{
		DamageValue += Health; // Decreace Damage by health that went under zero
		Health = 0.0f;
		bIsAlive = false;
		DeathEvent_Multicast();
	}
}

// Decreasing health
void UHealthComponent::IncreaseHealth_OnServer_Implementation(float HealValue)
{
	// If owner is dead or healing is zero, do nothing
	if (!bIsAlive || HealValue == 0)
	{
		return;
	}

	// Change health
	Health += HealValue;

	// Check if health didn't go above max value
	if (Health > MaxHealth)
	{
		HealValue -= Health - MaxHealth; // substract overhealth from Healing Done
		Health = MaxHealth;
	}

	OnHealthChangeEvent_Multicast(Health, HealValue);
}

// Check if owner is alive
bool UHealthComponent::IsAlive()
{
	return bIsAlive;
}

AController* UHealthComponent::GetLastDamageInstigator() const
{
	return LastDamageInstigator;
}

// Binded on owner's AnyDamageEvent;
void UHealthComponent::OwnerGotDamaged(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health > 0)
	{
		if (bIsAlive)
		{
			// Remember last damage instigator to calculate the score at death event
			LastDamageInstigator = InstigatedBy;
			DecreaseHealth_OnServer(Damage);
		}
	}
}

void UHealthComponent::OnHealthChangeEvent_Multicast_Implementation(float NewHealth, float ChangeValue)
{
	OnHealthChange.Broadcast(NewHealth, ChangeValue);
}

void UHealthComponent::DeathEvent_Multicast_Implementation()
{
	ACharacterBase* OwnerCharacter = Cast<ACharacterBase>(GetOwner());
	if (OwnerCharacter)
	{
		OwnerCharacter->OnDead();
	}

	// Broadcast death event for widget
	OnDead.Broadcast();
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);
	DOREPLIFETIME(UHealthComponent, bIsAlive);
	DOREPLIFETIME(UHealthComponent, LastDamageInstigator);
}