// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, ChangeValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JADESTUDIO_TESTGAME_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

	/** Delegate for health change (damage or heal) */
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnHealthChange OnHealthChange;

	/** Delegate for death */
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDead OnDead;

public:	
	// Sets default values for this component's properties
	UHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	/** Owner's max health */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", meta = (AllowPrivateAccess = "true"))
	float MaxHealth = 100.f;
	/** Owner's current health */
	UPROPERTY(Replicated)
	float Health;
	/** Is owner alive */
	UPROPERTY(Replicated)
	bool bIsAlive;

	/** Last damage causer - for calculating score at death */
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	AController* LastDamageInstigator;

public:
	/** Get current health */
	float GetHealth() const;
	/** Set current health */
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetHealth(float NewHealth);
	/** Decrease current health */
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	void DecreaseHealth_OnServer(float DamageValue);
	/** Increase current health */
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	void IncreaseHealth_OnServer(float HealValue);

	/** Check if owner is alive */
	bool IsAlive();

	// Get Last Damage Instigator
	AController* GetLastDamageInstigator() const;

protected:
	/** Damage event, binds on owner's OnTakeAnyDamage delegate */
	UFUNCTION()
	void OwnerGotDamaged(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	/** Health change multicast */
	UFUNCTION(NetMulticast, Reliable, Category = "Health")
	void OnHealthChangeEvent_Multicast(float NewHealth, float ChangeValue);

	/** Death multicast */
	UFUNCTION(NetMulticast, Reliable, Category = "Health")
	void DeathEvent_Multicast();
};
