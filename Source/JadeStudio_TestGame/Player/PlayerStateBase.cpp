// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStateBase.h"
#include "Net/UnrealNetwork.h"

EPlayerTeams APlayerStateBase::GetTeam() const
{
    return MyTeam;
}

void APlayerStateBase::SetTeam(EPlayerTeams NewTeam)
{
    MyTeam = NewTeam;
}

void APlayerStateBase::TeamChange_OnRep()
{
	UpdateStats();
}

void APlayerStateBase::UpdateStats_Implementation()
{
}

void APlayerStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerStateBase, MyTeam);
}

