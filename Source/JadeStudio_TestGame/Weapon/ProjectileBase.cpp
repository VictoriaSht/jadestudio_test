// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectileBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "JadeStudio_TestGame/Player/CharacterBase.h"
#include "Kismet/GameplayStatics.h"
#include "JadeStudio_TestGame/Player/PlayerStateBase.h"
#include "JadeStudio_TestGame/Effects/StatusEffect.h"
#include "Net/UnrealNetwork.h"

AProjectileBase::AProjectileBase()
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	//CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Overlap);
	CollisionComp->SetCanEverAffectNavigation(false);
	CollisionComp->OnComponentHit.AddDynamic(this, &AProjectileBase::OnHit);
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &AProjectileBase::OnBeginOverlap);

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 5000.f;
	ProjectileMovement->MaxSpeed = 5000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->OnProjectileStop.AddDynamic(this, &AProjectileBase::OnProjectileStop);

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	// Network
	bReplicates = true;
}

void AProjectileBase::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		Destroy();
	}
}

void AProjectileBase::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACharacterBase* Character = Cast<ACharacterBase>(OtherActor);
	if (Character)
	{
		ACharacter* DamagedCharacter = Cast<ACharacter>(SweepResult.GetActor());
		if (DamagedCharacter)
		{
			// Check if hit actor is enemy
			APlayerStateBase* OwnerPS = Cast<APlayerStateBase>(OwnerInfo);
			APlayerStateBase* DamagedPS = Cast<APlayerStateBase>(DamagedCharacter->GetPlayerState());
			
			if (OwnerPS && DamagedPS)
			{
				// If hit actor from other team, deal damage
				if (OwnerPS->GetTeam() != DamagedPS->GetTeam())
				{
					// Apply projectile effect
					if (ProjectileEffect)
					{
						UStatusEffect* NewEffect = NewObject<UStatusEffect>(DamagedCharacter, ProjectileEffect);
						if (NewEffect)
						{
							NewEffect->InitObject(DamagedCharacter, SweepResult.BoneName);
						}
					}
					// Apply damage
					UGameplayStatics::ApplyPointDamage(DamagedCharacter, ProjectileDamage, SweepResult.TraceStart, SweepResult, GetInstigatorController(), this, NULL);
					// Destroy projectile
					Destroy();
				}
			}
		}
	}
}

void AProjectileBase::OnProjectileStop(const FHitResult& Hit)
{
	Destroy();
}

void AProjectileBase::InitProjectile(AActor* inOwner, APlayerState* inOwnerInfo, float inProjectileDamage)
{
	OwnerInfo = inOwnerInfo;
	ProjectileDamage = inProjectileDamage;
}

void AProjectileBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AProjectileBase, ProjectileDamage);
}