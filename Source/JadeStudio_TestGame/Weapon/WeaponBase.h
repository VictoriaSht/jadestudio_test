// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "JadeStudio_TestGame/DataTypes/Enums.h"
#include "WeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadEnd, bool, bIsSuccess);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoChange, int32, NewAmmo);

UCLASS()
class JADESTUDIO_TESTGAME_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
	/** Skeletal mesh */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;

	/** Arrow shoot location */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

public:
	// Sets default values for this actor's properties
	AWeaponBase();

	UFUNCTION(Server, Unreliable)
	void SetShootLocation_OnServer(FVector InLocation);

	// Start reloading delegate
	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FOnWeaponReloadStart OnWeaponReloadStart;
	// End reloading delegate
	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	// Ammo change delegate
	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FOnAmmoChange OnAmmoChange;

	//* Weapon settings*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	float RateOfFire = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	float WeaponDamage = 25.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	float ReloadTime = 2.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	int32 MaxRound = 15;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	TSubclassOf<class AProjectileBase> ProjectileType = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	USoundBase* WeaponFireSound = nullptr;
	// For initiating pick up actor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings")
	EWeaponType WeaponType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);

	// Current ammo
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	int32 Round;

	// Weapon State
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "FireLogic")
	void SetWeaponStateFire_OnServer(bool bIsFire);
	// Check if weapon can fire right now
	bool CheckWeaponCanFire() const;

	// Is weapon firing
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bWeaponFiring = false;
	// Is firing blocked right now
	bool bBlockFire = false;

	// If weapon is reloading
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Reload Logic")
	bool bWeaponReloading = false;
	/** Get current weapon round */
	UFUNCTION(BlueprintCallable, Category = "Reload Logic")
	int32 GetWeaponRound() const;

	/** Get current weapon round */
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Reload Logic")
	void SetWeaponRound_OnServer(int32 inRound);

	// Attach weapon to character
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Weapon")
	void AttachWeapon_OnServer(class ACharacterBase* TargetCharacter);

	// Draw line to debug hitscan
	UFUNCTION(NetMulticast, Unreliable, Category = "Debug")
	void DrawHitscanLine_Multicast(FVector LineStart, FVector LineEnd);

	// Set attachment to GropPoint socket
	UFUNCTION(NetMulticast, Reliable)
	void AttachWeaponToSocket_Multicast(class ACharacterBase* inTargetCharacter, bool FirstPerson = true);

	// Set attachment to GropPoint socket on toggle view
	UFUNCTION(Server, Reliable)
	void ReAttachWeapon_OnServer(class ACharacterBase* inTargetCharacter, bool FirstPerson);

	// Initiate reloading
	void InitReload();
	// Reloading finished
	void FinishReload();
	// Cancel reloading
	void CancelReload();

private:

	// Fire weapon (calls only on server)
	void Fire();

	// Fire timer
	float FireTimer = 0.0f;
	// Reloading timer
	float ReloadTimer = 0.0f;

public:
	// Weapon fire sound event
	UFUNCTION(NetMulticast, Unreliable)
	void SoundWeaponFire_Multicast(USoundBase* inFireSound);

	// Ammo change event
	UFUNCTION(NetMulticast, Unreliable)
	void AmmoCountChange_Multicast(int32 NewAmmo);

	/** Reloading events */
	UFUNCTION(NetMulticast, Unreliable)
	void ReloadingStart_Multicast();
	UFUNCTION(NetMulticast, Unreliable)
	void ReloadingEnd_Multicast(bool Success);

private:
	/** Player state of player holding this weapon*/
	APlayerState* OwnerInfo;
	
};
