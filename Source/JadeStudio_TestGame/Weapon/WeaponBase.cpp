// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ProjectileBase.h"
#include "JadeStudio_TestGame/Player/CharacterBase.h"
#include "JadeStudio_TestGame/Player/PlayerStateBase.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
	// Set this actor to call Tick() every frame
	PrimaryActorTick.bCanEverTick = true;

	// Creating root component...
	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	// Creating skeletal mesh...
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("No Collision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	// Creating shoot location arrow...
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(SkeletalMeshWeapon);

	// Network
	bReplicates = true;

}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
	// Set round from weapon info
	Round = MaxRound;
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
	}
	if (GetOwner()->GetLocalRole() == ENetRole::ROLE_AutonomousProxy)
	{
		SetShootLocation_OnServer(ShootLocation->GetComponentLocation());
	}

}

void AWeaponBase::SetShootLocation_OnServer_Implementation(FVector InLocation)
{
	ShootLocation->SetWorldLocation(InLocation);
}

// Fire timer
void AWeaponBase::FireTick(float DeltaTime)
{
	if (bWeaponFiring && GetWeaponRound() > 0 && !bWeaponReloading)
	{
		if (FireTimer < 0.f)
		{
			if (!bWeaponReloading)
			{
				Fire();
			}
		}
		else
			FireTimer -= DeltaTime;
	}
}

// Reloading timer
void AWeaponBase::ReloadTick(float DeltaTime)
{
	if (bWeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

// Check if weapon can fire
bool AWeaponBase::CheckWeaponCanFire() const
{
	return !bBlockFire;
}

// Weapon State
void AWeaponBase::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
		bWeaponFiring = bIsFire;
	else
		bWeaponFiring = false;
}

// Get current round
int32 AWeaponBase::GetWeaponRound() const
{
	return Round;
}

void AWeaponBase::SetWeaponRound_OnServer_Implementation(int32 inRound)
{
	Round = inRound;
	AmmoCountChange_Multicast(Round);
}

void AWeaponBase::AttachWeapon_OnServer_Implementation(ACharacterBase* TargetCharacter)
{
	// Remember weapon owner
	OwnerInfo = TargetCharacter->GetPlayerState();

	Owner = TargetCharacter;
	SetInstigator(TargetCharacter->GetInstigator());

	// Bind events for reloading animation
	OnWeaponReloadStart.AddDynamic(TargetCharacter, &ACharacterBase::StartReloadingWeapon);
	OnWeaponReloadEnd.AddDynamic(TargetCharacter, &ACharacterBase::EndReloadingWeapon);

	// Check that the character is valid
	if (TargetCharacter == nullptr)
	{
		return;
	}

	// Attach the weapon to the Character
	AttachWeaponToSocket_Multicast(TargetCharacter);

	// switch bHasWeapon so the animation blueprint can switch to another animation set
	TargetCharacter->SetCurrentWeapon_OnServer(this);
}

void AWeaponBase::AttachWeaponToSocket_Multicast_Implementation(class ACharacterBase* inTargetCharacter, bool FirstPerson)
{
	if (inTargetCharacter)
	{
		FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
		if (FirstPerson && inTargetCharacter->IsLocallyControlled())
		{
			SkeletalMeshWeapon->AttachToComponent(inTargetCharacter->GetMesh1P(), AttachmentRules, FName(TEXT("GripPoint")));
		}
		else
		{
			SkeletalMeshWeapon->AttachToComponent(inTargetCharacter->GetMesh(), AttachmentRules, FName(TEXT("GripPoint")));
		}
	}
}

void AWeaponBase::ReAttachWeapon_OnServer_Implementation(ACharacterBase* inTargetCharacter, bool FirstPerson)
{
	AttachWeaponToSocket_Multicast(inTargetCharacter, FirstPerson);
}


void AWeaponBase::DrawHitscanLine_Multicast_Implementation(FVector LineStart, FVector LineEnd)
{
	DrawDebugLine(GetWorld(), LineStart, LineEnd, FColor::Red, false, 0.2f, (uint8)'\000', 0.5f);
}

void AWeaponBase::InitReload()
{
	// Calls only on server
	bWeaponReloading = true;
	ReloadTimer = ReloadTime;
	ReloadingStart_Multicast();
}

void AWeaponBase::FinishReload()
{
	// Calls only on server
	bWeaponReloading = false;
	Round = MaxRound;
	FireTimer = 0.0f;
	ReloadingEnd_Multicast(true);
}

void AWeaponBase::CancelReload()
{
	bWeaponReloading = false;
	ReloadingEnd_Multicast(false);
}

void AWeaponBase::ReloadingEnd_Multicast_Implementation(bool Success)
{
	OnWeaponReloadEnd.Broadcast(Success);
}

void AWeaponBase::ReloadingStart_Multicast_Implementation()
{
	OnWeaponReloadStart.Broadcast();
}

// Fire weapon (calls only on server)
void AWeaponBase::Fire()
{
	//  Calls only on server by FireTick

	// Sound and FX
	SoundWeaponFire_Multicast(WeaponFireSound);

	//Fire logic
	FireTimer = RateOfFire;
	Round--;
	AmmoCountChange_Multicast(Round);

	// Get shoot location
	FVector SpawnLocation = ShootLocation->GetComponentLocation();
	FRotator SpawnRotation = FRotator::ZeroRotator;
	APawn* MyOwner = Cast<APawn>(GetOwner());
	if (MyOwner)
	{
		SpawnRotation = MyOwner->GetController()->GetControlRotation();
	}


	if (ProjectileType)
	{
		/**  Projectile fire */

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();

		// Spawn projectile
		AProjectileBase* Projectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileType, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (Projectile)
			Projectile->InitProjectile(GetOwner(), OwnerInfo, WeaponDamage);
	}
	else
	{
		/** Hitscan */

		TArray<FHitResult> Hits;
		TArray<AActor*> Actors;

		FVector EndLocation = SpawnLocation + SpawnRotation.Vector() * 20000.0f;
		UKismetSystemLibrary::LineTraceMulti(GetWorld(), SpawnLocation, EndLocation, ETraceTypeQuery::TraceTypeQuery3, false, Actors, EDrawDebugTrace::None, Hits, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
		DrawHitscanLine_Multicast(SpawnLocation, EndLocation);

		for (auto elem : Hits)
		{
			if (elem.GetActor() && elem.GetActor() != OwnerInfo->GetPawn())
			{
				ACharacter* DamagedCharacter = Cast<ACharacter>(elem.GetActor());
				if (DamagedCharacter)
				{
					// Check if hit actor is enemy
					APlayerStateBase* OwnerPS = Cast<APlayerStateBase>(OwnerInfo);
					APlayerStateBase* DamagedPS = Cast<APlayerStateBase>(DamagedCharacter->GetPlayerState());

					if (OwnerPS && DamagedPS)
					{
						// If hit actor from other team, deal damage and stop search
						if (OwnerPS->GetTeam() != DamagedPS->GetTeam())
						{
							UGameplayStatics::ApplyPointDamage(elem.GetActor(), WeaponDamage, elem.TraceStart, elem, GetInstigatorController(), this, NULL);
							break;
						}
					}
				}
			}
		}
	}

	// Reloading
	if (GetWeaponRound() <= 0 && !bWeaponReloading)
	{
		InitReload();
	}
}

void AWeaponBase::AmmoCountChange_Multicast_Implementation(int32 NewAmmo)
{
	// Tell widget to change ammo count
	OnAmmoChange.Broadcast(NewAmmo);
}

void AWeaponBase::SoundWeaponFire_Multicast_Implementation(USoundBase* inFireSound)
{
	if (inFireSound)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), inFireSound, ShootLocation->GetComponentLocation());
}


void AWeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponBase, Round);
	DOREPLIFETIME(AWeaponBase, bWeaponReloading);
}

