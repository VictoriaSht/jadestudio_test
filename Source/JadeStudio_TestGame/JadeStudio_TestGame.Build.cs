// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class JadeStudio_TestGame : ModuleRules
{
	public JadeStudio_TestGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "Niagara" });
	}
}
