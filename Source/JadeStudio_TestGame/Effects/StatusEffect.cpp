// Fill out your copyright notice in the Description page of Project Settings.


#include "StatusEffect.h"
#include "JadeStudio_TestGame/Player/HealthComponent.h"
#include "JadeStudio_TestGame/Player/CharacterBase.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"


/////////////////////////////////////////Base////////////////////////////////////////////////////

bool UStatusEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	// Add effect to actor
	if (Actor)
	{
		AffectedActor = Actor;
		ACharacterBase* myPlayer = Cast<ACharacterBase>(AffectedActor);
		if (myPlayer)
		{
			myPlayer->AddEffect(this);
		}
	}
	//NameBone = NameBoneHit;
	return true;
}

void UStatusEffect::DestroyObject()
{
	// Remove effect from actor
	if (AffectedActor)
	{
		ACharacterBase* myPlayer = Cast<ACharacterBase>(AffectedActor);
		if (myPlayer)
		{
			myPlayer->RemoveEffect(this);
		}
	}
	// Clear and destroy object
	AffectedActor = nullptr;
	if (this && IsValidLowLevel())
	{
		ConditionalBeginDestroy();
	}
	OnDestroyed.Broadcast();
}

float UStatusEffect::GetDuration()
{
	return -1.0f;
}

void UStatusEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UStatusEffect, NameBone);
}

/////////////////////////////////////////ExecuteOnce////////////////////////////////////////////////////

bool UStatusEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	if (Super::InitObject(Actor, NameBoneHit))
	{
		InitialExecute();
		return true;
	}
	return false;
}

void UStatusEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStatusEffect_ExecuteOnce::InitialExecute()
{
}

/////////////////////////////////////////WithDuration////////////////////////////////////////////////////

bool UStatusEffect_WithDuration::InitObject(AActor* Actor, FName NameBoneHit)
{
	if (Super::InitObject(Actor, NameBoneHit))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStatusEffect_WithDuration::DestroyObject, EffectTime);
		return true;
	}
	return false;
}

void UStatusEffect_WithDuration::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	Super::DestroyObject();
}

float UStatusEffect_WithDuration::GetDuration()
{
	return EffectTime;
}

/////////////////////////////////////////ExecuteTimer////////////////////////////////////////////////////

bool UStatusEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	if (Super::InitObject(Actor, NameBoneHit))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStatusEffect_ExecuteTimer::Execute, ExecuteTime, true);
		Execute();
		return true;
	}

	return false;
}

void UStatusEffect_ExecuteTimer::DestroyObject()
{
	Super::DestroyObject();
}

void UStatusEffect_ExecuteTimer::Execute()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////SUBCLASSES////////////////////////////////////////////////////
/////////////////////////////////ExecuteTimer_HealthInfluence//////////////////////////////////////////

void UStatusEffect_ExecuteTimer_HealthInfluence::Execute()
{
	if (AffectedActor)
	{
		UHealthComponent* myHealthComponent = Cast<UHealthComponent>(AffectedActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			if (Power > 0)
				myHealthComponent->IncreaseHealth_OnServer(Power);
			else if (Power < 0)
				myHealthComponent->DecreaseHealth_OnServer(-Power);
		}
	}
	else
		DestroyObject();
}

