// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "StatusEffect.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDestroyed);
/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class JADESTUDIO_TESTGAME_API UStatusEffect : public UObject
{
	GENERATED_BODY()
	
protected:
    // Actor, affected by this effect
    AActor* AffectedActor = nullptr;

public:
    // For replication
    virtual bool IsSupportedForNetworking() const override { return true; };
    // Init effect
    virtual bool InitObject(AActor* Actor, FName NameBoneHit);
    // Destroy effect
    virtual void DestroyObject();

    // Delegate for destroy event
    FOnDestroyed OnDestroyed;

    // Get effect duration
    virtual float GetDuration();

    // Bone to attach effect's FX
    UPROPERTY(Replicated)
    FName NameBone;
};

/////////////////////////////////////////ExecuteOnce////////////////////////////////////////////////////
UCLASS()
class JADESTUDIO_TESTGAME_API UStatusEffect_ExecuteOnce : public UStatusEffect
{
    GENERATED_BODY()

public:
    bool InitObject(AActor* Actor, FName NameBoneHit) override;
    void DestroyObject() override;

    // Initial effect impact
    virtual void InitialExecute();

    // Power of effect
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
    float Power = 20.0f;

    //* Initial FX (plays once) */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    class UNiagaraSystem* InitialEffect = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    FVector InitialEffectOffset = FVector::ZeroVector;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    FVector InitialEffectScale = FVector(1.0f, 1.0f, 1.0f);

    //* Initial sound (plays once) */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    USoundBase* InitialSound = nullptr;

};

/////////////////////////////////////////WithDuration////////////////////////////////////////////////////
UCLASS()
class JADESTUDIO_TESTGAME_API UStatusEffect_WithDuration : public UStatusEffect_ExecuteOnce
{
    GENERATED_BODY()

public:
    bool InitObject(AActor* Actor, FName NameBoneHit) override;
    void DestroyObject() override;
    float GetDuration() override;

    // Effect duration time
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting With Duration")
    float EffectTime = 5.0f;

    //* Duration FX */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    class UNiagaraSystem* DurationEffect = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    FVector DurationEffectOffset = FVector::ZeroVector;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    FVector DurationEffectScale = FVector(1.0f, 1.0f, 1.0f);

    //* Duration sound */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    USoundBase* DurationSound = nullptr;

    // Sound component
   // class UAudioComponent* DurationAudioComponent = nullptr;

    // Effect timer
    FTimerHandle TimerHandle_EffectTimer;
};

/////////////////////////////////////////ExecuteTimer////////////////////////////////////////////////////
UCLASS()
class JADESTUDIO_TESTGAME_API UStatusEffect_ExecuteTimer : public UStatusEffect_WithDuration
{
    GENERATED_BODY()

public:
    bool InitObject(AActor* Actor, FName NameBoneHit) override;
    void DestroyObject() override;

    // Execute timer effect impact
    virtual void Execute();

    // How frequently effect impacts actor
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
    float ExecuteTime = 1.0f;

    // Execute timer
    FTimerHandle TimerHandle_ExecuteTimer;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////SUBCLASSES////////////////////////////////////////////////////

/////////////////////////////////ExecuteTimer_HealthInfluence//////////////////////////////////////////
UCLASS()
class JADESTUDIO_TESTGAME_API UStatusEffect_ExecuteTimer_HealthInfluence : public UStatusEffect_ExecuteTimer
{
    GENERATED_BODY()

public:
    void Execute() override;
};
