#pragma once

#include "Enums.generated.h"

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	WT_TestBalisticWeapon UMETA(DisplayName = "Test Balistic Weapon"),
	WT_TestHitscanWeapon UMETA(DisplayName = "Test Hitscan Weapon")
};

UENUM(BlueprintType)
enum class EPlayerTeams : uint8
{
	PT_NONE UMETA(DisplayName = "Team NONE"),
	PT_RedTeam UMETA(DisplayName = "Red Team"),
	PT_BlueTeam UMETA(DisplayName = "Blue Team")
};