// Copyright Epic Games, Inc. All Rights Reserved.

#include "JadeStudio_TestGame/Core/GameModeBaseNew.h"
#include "JadeStudio_TestGame/Player/CharacterBase.h"
#include "UObject/ConstructorHelpers.h"

AGameModeBaseNew::AGameModeBaseNew()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
