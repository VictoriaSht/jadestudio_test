// Copyright Epic Games, Inc. All Rights Reserved.

#include "JadeStudio_TestGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JadeStudio_TestGame, "JadeStudio_TestGame" );
 